//index es mi servidor

const express = require("express");
const bodyParser = require("body-parser");
require("dotenv").config();

//CReando aplicación express
const app = express();

// bpdy-parser (usando el middleware)
app.use(bodyParser.urlencoded({ extended: true }));

// Ruta
app.use("/", require("./routes/auth"));

app.listen(process.env.PORT || 3000, () => {
  const port = process.env.PORT || 3000;
  // backquote - >    option + }  ```
  console.log(`Servidor corriendo en el puerto -> ${port}`);
});

const { OAuth2Client } = require("google-auth-library");

const CLIENT_ID =
  "186259966147-mdoqiauk10ad7230sd9umuurotsof539.apps.googleusercontent.com";

const CLIENT_ID_IOS =
  "186259966147-ljlmhgdauldqi922m16uiemjcihd2iar.apps.googleusercontent.com";

const CLIENT_ID_ANDROID =
  "186259966147-cqm3m1gs89pmj7d6jbd577l8qa7elcd5.apps.googleusercontent.com";

const client = new OAuth2Client();

// Es lo mismo... solo que son funciones con Nombre
const validateGoogleIdToken = async (token) => {
  try {
    const ticket = await client.verifyIdToken({
      idToken: token,
      audience: [CLIENT_ID, CLIENT_ID_ANDROID, CLIENT_ID_IOS], // Specify the CLIENT_ID of the app that accesses the backend
      // Or, if multiple clients access the backend:
      //[CLIENT_ID_1, CLIENT_ID_2, CLIENT_ID_3]
    });
    const payload = ticket.getPayload();

    console.log(payload);
    const userid = payload["sub"];

    return {
      name: payload["name"],
      family_name: payload["family_name"],
      email: payload["email"],
      picture: payload["picture"],
    };
    // If request specified a G Suite domain:
    // const domain = payload['hd'];
  } catch (error) {
    console.log(error);
    return null;
  }
};

// async function verify() {
//   const ticket = await client.verifyIdToken({
//     idToken: token,
//     audience: CLIENT_ID, // Specify the CLIENT_ID of the app that accesses the backend
//     // Or, if multiple clients access the backend:
//     //[CLIENT_ID_1, CLIENT_ID_2, CLIENT_ID_3]
//   });
//   const payload = ticket.getPayload();
//   const userid = payload["sub"];
//   // If request specified a G Suite domain:
//   // const domain = payload['hd'];
// }
// verify().catch(console.error);

module.exports = { validateGoogleIdToken };

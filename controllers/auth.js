const { response } = require("express");
const { validateGoogleIdToken } = require("../helpers/google-verify-token");

const googleAuth = async (req, res = response) => {
  const token = req.body.token;

  if (!token) {
    return res.json({
      ok: false,
      msg: "No hay token en la petición",
    });
  }

  const googleUser = await validateGoogleIdToken(token);

  if (!googleUser) {
    return res.status(400).json({
      ok: false,
    });
  }

  // TODO en este punto aca ya se puede guardar en la base de datos

  res.json({
    ok: true,
    googleUser,
  });
};

// exportacion por nombres..
module.exports = { googleAuth };
